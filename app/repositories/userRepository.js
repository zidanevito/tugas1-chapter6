const { User } = require("../models");

const bcrypt = require("bcrypt");

module.exports = {
  create(createArgs) {
    return User.create(createArgs);
  },

  update(id, updateArgs) {
    return User.update(updateArgs, {
      where: {
        id,
      },
    });
  },

  delete(id) {
    return User.destroy(id);
  },

  find(id) {
    return User.findByPk(id, { attributes: { exclude: ["password"] } });
  },

  // hide password field
  findAll() {
    return User.findAll({ attributes: { exclude: ["password"] } });
  },

  getTotalUser() {
    return User.count();
  },

  // register
  async registerNewUser(createArgs) {
    console.log("ini repository");

    const user = await User.findOne({
      where: {
        email: createArgs.email,
      },
    });

    // check if user not exist
    if (user) {
      if (user.email === createArgs.email) {
        console.log("masukkk");
        throw new Error(`user with email : ${user.email} already taken`);
      }
    }

    console.log("iinnniiiii");

    return User.create(createArgs);
  },

  //
  async login(userArgs) {
    // console.log(userArgs)
    const user = await User.findOne({
      where: {
        email: userArgs.email,
      },
    });
    if (user) {
      const verify = await bcrypt.compare(userArgs.password, user.password);
      // console.log(user.password)
      if (verify) {
        return User.findOne({
          where: {
            email: userArgs.email,
          },
        });
      } else {
        throw new Error("Password Salah");
      }
    }
    throw new Error("User Tidak ditemukan");
  },
};

// module.exports = {
//   create
//   register
//   fafwfwfw
// }
