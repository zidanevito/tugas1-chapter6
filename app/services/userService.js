const bcrypt = require('bcrypt');

const userRepository = require("../repositories/userRepository");

module.exports = {
  create(requestBody) {
    return userRepository.create(requestBody);
  },

  update(id, requestBody) {
    return userRepository.update(id, requestBody);
  },

  delete(id) {
    return userRepository.delete(id);
  },

  async list() {
    try {
      const posts = await userRepository.findAll();
      const postCount = await userRepository.getTotalUser();

      return {
        data: posts,
        count: postCount,
      };
    } catch (err) {
      throw err;
    }
  },

  get(id) {
    return userRepository.find(id);
  },

  register(requestBody) {
    console.log('ini services')
    
    const hash = bcrypt.hashSync(requestBody.password, 10);

    const reqBody = {
      firstName: requestBody.firstName,
      lastName: requestBody.lastName,
      email: requestBody.email,
      password: hash
    }

    return userRepository.registerNewUser(reqBody);
  },

  login(requestBody) {
    return userRepository.login(requestBody)
  }
};
